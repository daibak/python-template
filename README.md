# python-template

## Description
Python template for all reasonable projects: from library, to application, to ML experiments.
It will use the latest standards in the python ecosystem with some arbitrary choices for more specific applications like ML development.

In order to be minimal but versatile it has a composable structure: using VCS branches to build on top of each other features:

- The main branch is a minimal [PyPI](https://pypi.org/) compliant package for simple python code.
- Other early branches build upon it, possibly in a composable way. 
- Finally some branches are the combinations of many others, and are ready to be deployed as templates for specific applications.

As the python ecosystem moves forward each branch will be updated and rebased, listing its main blocks in the README. 
A template library ([copier](https://copier.readthedocs.io/en/latest/)) is used underneath to easily upgrade also existing projects.

This templates should be python installation and virtual environment independent, however they are developed with [pyenv](https://github.com/pyenv/pyenv) and [venv](https://github.com/pyenv/pyenv-virtualenv) in mind, with packages from pip and not conda, unless specified otherwise in a branch.

## Usage
Install [copier](https://copier.readthedocs.io/en/latest/) as per their docs: hint `pipx install copier`.
Also extensions could be need, installed with: `pipx inject copier copier-templates-extensions python-slugify requests`,
you can find more in `requirements/dev.in`.

[Generate](https://copier.readthedocs.io/en/latest/generating/) a project using a branch specific URL, for example:
```bash
copier copy --trust --vcs-ref main https://gitlab.com/daibak/python-template.git ./path/to/destination
```

Each branch has a [`TEMPLATE_INFO.md`](TEMPLATE_INFO.md) file with all information regarding usage, components and 3rd party libs in their template.

## Templates (& Branches)

- [main](https://gitlab.com/daibak/python-template/-/blob/main/TEMPLATE_INFO.md): Minimal PyPI package
- [pip-tools](https://gitlab.com/daibak/python-template/-/blob/pip-tools/TEMPLATE_INFO.md): Minimal + [pip-tools](https://github.com/jazzband/pip-tools) best practices

## Support
For support open an issue, or better yet ask the internet.

## Roadmap
Ideally we will have as ready to use templates:

- Minimal PyPI package
- Pip-tools integration for reproducibility, with dependencies (including optionals) from file
- Additional Makefile workflow (different tools from `make` eventually)
- Minimal [Hatch](https://hatch.pypa.io/latest/) variant
- Hatch with pip-tools if possible ([Hatch has no lock file yet](https://hatch.pypa.io/latest/meta/faq/#libraries-vs-applications))
- Hydra config modules

## Contributing
Contributions are welcome with PRs on the relevant branches and possibly new branches for new components or specific applications.

Keep the new branches and PRs small though, templates cannot be tested automatically for now, check twice that everything is alright and acceptance will be slow because of manual tests.

Read `copier` documentation for good measure and look around in all our branches to understand how we do it. 

## Authors and acknowledgment
Only [daibak](main@daibak.me) for now.

## License
MIT licensed for simplicity.