from urllib import response

import requests
from jinja2.ext import Extension


def get_license_gitlab(
    license_key: str, project: str | None = None, owner: str | None = None
) -> str:
    if license_key == "custom":
        return f"Custom License file here for {project} by {owner}"
    api_root = "https://gitlab.com/api/v4/templates/licenses/"
    response = requests.get(
        api_root + license_key, params=dict(project=project, fullname=owner)
    )
    if response.ok:
        return response.json()["content"]
    return f"Failed to retrieve license: {response.status_code}: {response.reason}"


def get_license_github(
    license_key: str, project: str | None = None, owner: str | None = None
) -> str:
    if license_key == "custom":
        return f"Custom License file here for {project} by {owner}"
    api_root = "https://api.github.com/licenses/"
    response = requests.get(api_root + license_key)
    if response.ok:
        return response.json()["content"]
    return f"Failed to retrieve license: {response.status_code}: {response.reason}"


class LicenseGitLabExtension(Extension):
    """Jinja2 Extension to slugify string."""

    def __init__(self, environment):
        """Jinja2 Extension constructor."""
        super().__init__(environment)

        def get_license(value, **kwargs):
            return get_license_gitlab(value, **kwargs)

        environment.globals.update(get_license=get_license)


class LicenseGitHubExtension(Extension):
    """Jinja2 Extension to slugify string."""

    def __init__(self, environment):
        """Jinja2 Extension constructor."""
        super().__init__(environment)

        def get_license(value, **kwargs):
            return get_license_github(value, **kwargs)

        environment.globals.update(get_license=get_license)
