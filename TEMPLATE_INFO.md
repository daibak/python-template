## Template Description

Minimal PyPI package following:
- PEP [517](https://peps.python.org/pep-0517/)/[518](https://peps.python.org/pep-0518/) for build systems
- PEP [621](https://peps.python.org/pep-0621/)/[631](https://peps.python.org/pep-0631/) for metadata in `pyproject.toml`
- PEP [440](https://peps.python.org/pep-0440/#public-version-identifiers) for version handling
- PEP [660](https://peps.python.org/pep-0660/) compliant `editable install`

It builds upon:
- [setuptools](https://pypi.org/project/setuptools/)
- [setuptools-scm](https://pypi.org/project/setuptools-scm/)

It is so minimal it does not have test or type checking.

## Usage

### Build
Build with `python -m build`, docs at [build](https://github.com/pypa/build).

### Dependencies
Remeber to specify dependencies with PEP [508](https://peps.python.org/pep-0508/) strings in the `pyproject.toml` file.